import React from 'react';
import PropTypes from 'prop-types';

import {default as ReactYouTube} from 'react-youtube';

import Video from 'video';

import './YouTube.scss';

/**
 * YouTube
 * @description [Description]
 * @example
  <div id="YouTube"></div>
  <script>
    ReactDOM.render(React.createElement(Components.YouTube, {
        title : 'Example YouTube'
    }), document.getElementById("YouTube"));
  </script>
 */
class YouTube extends Video {
	constructor(props) {
		super(props);

		const {previewImageUrl, videoUrl} = this.props;

		this.videoId = videoUrl.indexOf('v=') !== -1 ? videoUrl.split('v=')[1] : videoUrl;
		this.previewImage = previewImageUrl
			? previewImageUrl
			: `https://img.youtube.com/vi/${this.videoId}/maxresdefault.jpg`;

		this.state = {
			player: null,
			vOpacity: 0,
			playing: false,
			playCount: 0,
			isInline: props.isInline
		};

		//this.onReady = this.onReady.bind(this);
		//this.onStateChange = this.onStateChange.bind(this);
	}

	onPlay = () => {
		this.setState(
			{
				playing: true,
				playCount: this.state.playCount + 1
			},
			() => {
				this.stopOtherVideos();
			}
		);

		if (this.state.isInline) {
			if (this.state.player) {
				this.state.player.playVideo();
			} else {
				this.playOnLoad = true;
			}
		}
	};

	onClose = () => {
		this.setState({vOpacity: 0, playing: false});
	};

	onReady = e => {
		this.setState(
			{
				player: e.target,
				vOpacity: 0
			},
			() => {
				if (!this.state.isInline || this.playOnLoad) {
					this.state.player.playVideo();
				}
			}
		);
	};

	onStateChange = e => {
		if (e.data === 1) {
			this.setState({vOpacity: 1, playing: true});
		} else if (e.data === -1) {
			this.setState({vOpacity: 0});
		} else if (e.data === 2) {
			this.setState({playing: false});
		} else if (e.data === 0) {
			this.setState({playing: false});
		}
	};

	renderVideo() {
		const opts = {
			height: '100%',
			width: '100%',
			playerVars: {
				color: '#ffffff',
				controls: 1,
				enablejsapi: 1,
				fs: 0,
				iv_load_policy: 0,
				loop: 0,
				modestbranding: 1,
				playsinline: 1,
				rel: 0,
				showinfo: 0
			}
		};

		return (
			<ReactYouTube
				opts={opts}
				videoId={this.videoId}
				onReady={this.onReady}
				onStateChange={this.onStateChange}
				containerClassName={`${this.baseClass}__wrapper`}
			/>
		);
	}
}

export default YouTube;
